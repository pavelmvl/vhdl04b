----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.04.2014 21:11:03
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top is
    Port (
        clk   : in     std_logic;
        en    : in     std_logic;
        rst   : in     std_logic;
        rx    : in     std_logic;
        tx    :   out  std_logic;
        tx_str: in     std_logic;
--        led   :   out  std_logic_vector(7 downto 0)
        led   :   out  std_logic
    );
end top;

architecture Behavioral of top is

    component uart_rx is
        Generic (
            clk_Hz   : integer := 100_000_000;
            BaudRate : integer := 9_600
        );
        Port (
            clk    : in     std_logic; -- clock clk_Hz
            en     : in     std_logic; -- enable '1'
            rst    : in     std_logic; -- sync reset '1'
            -- receive part
            rx     : in     std_logic; -- receive wire
            rx_reg :   out  std_logic_vector(7 downto 0); -- received register
            rx_en  : in     std_logic; -- receive valid
            rx_chk :   out  std_logic  -- recive byte complite successful
        );
    end component uart_rx;

    component uart_tx is
        Generic (
            clk_Hz   : integer;
            BaudRate : integer
        );
        Port (
            clk    : in     std_logic; -- clock clk_Hz
            en     : in     std_logic; -- enable '1'
            rst    : in     std_logic; -- sync reset '1'
            -- transmit part
            tx     :   out  std_logic; -- transmit wire
            tx_reg : in     std_logic_vector(7 downto 0); -- transmit register
            tx_str : in     std_logic; -- start transmitting
            tx_rdy :   out  std_logic  -- ready to transmit
        );
    end component uart_tx;

    signal rx_reg    : std_logic_vector(7 downto 0);
    signal rx_en     : std_logic;
    signal rx_chk    : std_logic;
    
    signal tx_reg    : std_logic_vector(7 downto 0);
--    signal tx_str    : std_logic;
    signal tx_rdy    : std_logic;

begin

--    tx_reg <= x"42";
    tx_reg <= rx_reg;
--    tx_str <= rx_chk;
    rx_en  <= '1';
--    led <= tx_reg;
    led <= tx_str;
    
uart_tx_inst: uart_tx
    generic map(100_000_000, 9_600)
    port map(clk, en, rst, tx, tx_reg, tx_str, tx_rdy);

uart_rx_inst: uart_rx
    generic map(100_000_000, 9_600)
    port map(clk, en, rst, rx, rx_reg, rx_en, rx_chk);

end Behavioral;
