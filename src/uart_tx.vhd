----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.04.2014 22:06:29
-- Design Name: 
-- Module Name: uart_tx - RTL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:0.20
-- Revision 0.01 - File Created
-- Revision 0.10 - ISE model worked
-- Revision 0.20
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uart_tx is
    Generic (
        clk_Hz   : integer := 100_000_000;
        BaudRate : integer := 9_600
    );
    Port (
        clk    : in     std_logic; -- clock clk_Hz
        en     : in     std_logic; -- enable '1'
        rst    : in     std_logic; -- sync reset '1'
        -- transmit part
        tx     :   out  std_logic; -- transmit wire
        tx_reg : in     std_logic_vector(7 downto 0); -- transmit register
        tx_str : in     std_logic; -- start transmitting
        tx_rdy :   out  std_logic  -- ready to transmit
    );
end uart_tx;

architecture RTL of uart_tx is

    -- TRANSMIT VARIABLES and TYPES
    type TTxState is (IDLE, START, TRANSMIT);
    
    constant BaudCount_byte     : integer := clk_Hz * 10 / BaudRate;
    constant BaudCount_bit      : integer := clk_Hz / BaudRate;
    constant BaudCount_half_bit : integer := BaudCount_bit / 2;

    signal tx_state    : TTxState;
    signal tx_shift    : std_logic_vector(9 downto 0);
    signal tx_shift_CE : std_logic;

    signal count_bit   : integer range 0 to BaudCount_bit;
    signal count_byte  : integer range 0 to BaudCount_byte;

begin

    -- TRANSMIT RTL PART

    tx <= tx_shift(0);

tx_shift_reg:process(en, rst, clk, tx_shift_CE)
    begin
        if en = '1' then
            if rst = '1' then
                tx_shift <= (others => '1');
            elsif rising_edge(clk) then
                if tx_state = START then
                    tx_shift <= tx_reg & '0' & '1';
                elsif tx_shift_CE = '1' then
                    tx_shift <= '1' & tx_shift(9 downto 1);
                end if;
            end if;
        end if;
    end process;

tx_counter_byte:process(en, rst, clk, tx_state)
    begin
        if en = '1' then
            if rst = '1' or tx_state = IDLE then
                count_byte <= 0;
            elsif falling_edge(clk) then
                if count_byte = BaudCount_byte - 1 then
                    count_byte <= 0;
                else
                    count_byte <= count_byte + 1;
                end if;
            end if;
        end if;
    end process;

tx_counter_bit:process(en, rst, clk, tx_state)
    begin
        if en = '1' then
            if rst = '1' or tx_state = IDLE then
                count_bit <= 0;
            elsif falling_edge(clk) then
                if count_bit = BaudCount_bit - 1 then
                    count_bit <= 0;
                else
                    count_bit <= count_bit + 1;
                end if;
            end if;
        end if;
    end process;

tx_shift_CE_maker:process(en, rst, clk, tx_state)
    begin
        if en = '1' then
            if rst = '1' or tx_state = IDLE then
                tx_shift_CE <= '0';
            elsif falling_edge(clk) then
                if count_bit = BaudCount_bit - 1 then
                    tx_shift_CE <= '1';
                else
                    tx_shift_CE <= '0';
                end if;
            end if;
        end if;
    end process;

tx_statter:process(en, rst, clk)
    begin
        if en = '1' then
            if rst = '1' then
                tx_state <= IDLE;
            elsif rising_edge(clk) then
                case tx_state is
                    when START    =>
                        if count_byte >= 10 then
                            tx_state <= TRANSMIT;
                        end if;
                    when TRANSMIT =>
                        if count_byte = BaudCount_byte - 1 then
                            tx_state <= IDLE;
                            tx_rdy <= '1';
                        else
                            tx_rdy <= '0';
                        end if;
                    when IDLE     =>
                        tx_rdy <= '1';
                        if tx_str = '1' then
                            tx_state <= START;
                        end if;
                    when others   =>
                        tx_rdy <= '0';
                        tx_state <= IDLE;
                end case;
            end if;
        end if;
    end process;

end RTL;
