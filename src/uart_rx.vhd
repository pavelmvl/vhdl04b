----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.04.2014 22:06:29
-- Design Name: 
-- Module Name: uart_rx - RTL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:0.10
-- Revision 0.01 - File Created
-- Revision 0.10 - ISE model worked
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uart_rx is
    Generic (
        clk_Hz   : integer := 100_000_000;
        BaudRate : integer := 9_600
    );
    Port (
        clk    : in     std_logic; -- clock clk_Hz
        en     : in     std_logic; -- enable '1'
        rst    : in     std_logic; -- sync reset '1'
        -- receive part
        rx     : in     std_logic; -- receive wire
        rx_reg :   out  std_logic_vector(7 downto 0); -- received register
        rx_en  : in     std_logic; -- receive valid
        rx_chk :   out  std_logic  -- recive byte complite successful
    );
end uart_rx;

architecture RTL of uart_rx is

    -- RECEIVE VARIABLES and TYPES
    type TRxState is (START, RECEIVE, STOP, IDLE, DISABLED);

    constant BaudCount      : integer := clk_Hz / BaudRate;
    constant BaudCount_half : integer := BaudCount / 2;
    
    signal rx_state   : TRxState;
    
    signal count_bit  : integer range 0 to BaudCount;
    signal count_recv : integer range 0 to 9;
    
    signal rx_delayed : std_logic;
    signal rx_falling : std_logic;
    
    signal rx_shift   : std_logic_vector(7 downto 0);
    signal rx_shift_CE: std_logic;

begin

    -- RECEIVE RTL PART
    
    rx_falling <= '1' when rx_delayed = '1' and rx = '0' else '0';
    
rx_shift_reg:process(en, rst, clk, rx, rx_state, rx_shift_CE)
    begin
        if en = '1' then
            if rst = '1' or rx_state = IDLE then
--                rx_shift   <= (others => '1');
                rx_chk     <= '0';
                count_recv <= 0;
            elsif rising_edge(clk) then
                if rx_state = STOP and count_bit = BaudCount_half then
                    rx_reg <= rx_shift;
                    if rx = '1' then
                        rx_chk <= '1';
                    else
                        rx_chk <= '0';
                    end if;
                end if;
                if rx_shift_CE = '1' then
                    rx_shift <= rx & rx_shift(7 downto 1);
                    count_recv <= count_recv + 1;
                end if;
            end if;
        end if;
    end process;

rx_counter_bit:process(en, rst, clk, rx_state)
    begin
        if en = '1' then
            if rst = '1' or rx_state = IDLE then
                count_bit <= 0;
            elsif falling_edge(clk) then
                if count_bit = BaudCount - 1 then
                    count_bit <= 0;
                else
                    count_bit <= count_bit + 1;
                end if;
            end if;
        end if;
    end process;

    rx_shift_CE <= '1' when rx_state = RECEIVE and count_bit = BaudCount_half and en = '1' and rst = '0' else '0';

--rx_shift_CE_maker:process(en, rst, clk, rx_state)
--    begin
--        if en = '1' then
--            if rst = '1' or rx_state = IDLE then
--                rx_shift_CE <= '0';
--            elsif rising_edge(clk) then
--                if rx_state = RECEIVE then
--                    if count_bit = BaudCount_half then
--                        rx_shift_CE <= '1';
--                    else
--                        rx_shift_CE <= '0';
--                    end if;
--                else
--                    rx_shift_CE <= '0';
--                end if;
--            end if;
--        end if;
--    end process;

rx_statter:process(en, rst, clk)
    begin
        if en = '1' then
            if rst = '1' then
                rx_state <= DISABLED;
            elsif rising_edge(clk) then
                case (rx_state) is
                    when START =>
                        if rx = '0' and count_bit = BaudCount_half then
                            rx_state <= RECEIVE;
                        elsif rx = '0' and count_bit = BaudCount_half then
                            rx_state <= IDLE;
                        end if;
                    when RECEIVE =>
                        if count_recv = 8 and count_bit = BaudCount - 1 then
                            rx_state <= STOP;
                        end if;
                    when STOP =>
                        if count_bit = BaudCount - 1 then
                            rx_state <= IDLE;
                        end if;
                    when IDLE =>
                        if rx_en = '1' and rx_falling = '1' then
                            rx_state <= START;
                        elsif rx_en = '0' then
                            rx_state <= DISABLED;
                        end if;
                        rx_delayed <= rx;
                    when DISABLED =>
                        if rx_en <= '1' then
                            rx_state <= IDLE;
                        end if;
                    when others =>
                        rx_state <= DISABLED;
                end case;
            end if;
        end if;
    end process;
    
end RTL;
