----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.04.2014 22:51:49
-- Design Name: 
-- Module Name: top_Behavioral - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_Behavioral is
end top_Behavioral;

architecture Behavioral of top_Behavioral is

    component uart_rx is
        Generic (
            clk_Hz   : integer := 100_000_000;
            BaudRate : integer := 9_600
        );
        Port (
            clk    : in     std_logic; -- clock clk_Hz
            en     : in     std_logic; -- enable '1'
            rst    : in     std_logic; -- sync reset '1'
            -- receive part
            rx     : in     std_logic; -- receive wire
            rx_reg :   out  std_logic_vector(7 downto 0); -- received register
            rx_en  : in     std_logic; -- receive valid
            rx_chk :   out  std_logic  -- recive byte complite successful
        );
    end component uart_rx;
    
    component uart_tx is
        Generic (
            clk_Hz   : integer;
            BaudRate : integer
        );
        Port (
            clk    : in     std_logic; -- clock clk_Hz
            en     : in     std_logic; -- enable '1'
            rst    : in     std_logic; -- sync reset '1'
            -- transmit part
            tx     :   out  std_logic; -- transmit wire
            tx_reg : in     std_logic_vector(7 downto 0); -- transmit register
            tx_str : in     std_logic; -- start transmitting
            tx_rdy :   out  std_logic  -- ready to transmit
        );
    end component uart_tx;

    constant PERIOD : time := 10 ns;

    signal en     : std_logic;
    signal rst    : std_logic;
    signal clk    : std_logic := '1';
    
    signal tx     : std_logic;
    signal tx_reg : std_logic_vector(7 downto 0);
    signal tx_str : std_logic;
    signal tx_rdy : std_logic;
    
    signal rx     : std_logic;
    signal rx_reg : std_logic_vector(7 downto 0);
    signal rx_en  : std_logic;
    signal rx_chk : std_logic;


begin

    clk <= not clk after PERIOD / 2;

signals:process
    begin
        en <= '0';
        rst <= '1';
        tx_str <= '0';
        tx_reg <= "01111110";
        rx_en <= '0';
        wait for PERIOD;
        en <= '1';
        wait for PERIOD;
        rst <= '0';
        wait for PERIOD;
        rx_en <= '1';
        wait;
    end process;

--receive_inst: uart_rx
--    generic map (100_000_000, 9_600)
--    port map (clk, en, rst, rx, rx_reg, rx_en, rx_rdy);

transmit_inst:uart_tx
    generic map (100_000_000, 9_600)
    port map (clk, en, rst, tx, tx_reg, tx_str, tx_rdy);

uart_rx_inst: uart_rx
    generic map(100_000_000, 9_600)
    port map(clk, en, rst, rx, rx_reg, rx_en, rx_chk);


    rx     <= tx;
    rx_en  <= '1';

end Behavioral;
